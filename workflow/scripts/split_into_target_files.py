import os

with open(snakemake.input[0]) as f:
    for line in f.readlines():
        if line.find('>') >= 0:
            label = line.split('>')[1].split(' ')[0]
        else:
            seq_fname = 'results/splitted_seq_files/'
#            seq_fname += snakemake.wildcards.filename+'_'+
            seq_fname += label + '.seq'
            outfile = open(seq_fname,'w')
            outfile.write(line)         
            outfile.close()

os.system("touch "+snakemake.output[0])        
