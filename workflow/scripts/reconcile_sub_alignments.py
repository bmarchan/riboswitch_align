overlap = snakemake.config["overlap_antiterminator"]

# labels
labels = []
for line in open(snakemake.input.terminator_align).readlines():
    if line.find('NC') >= 0 and line.find('GS') <0:
        labels.append(line.split(' ')[0])

# aptamer consensus structure and start_read
SS_cons_apta = ""
RF_apta = ""
for line in open(snakemake.input.aptamer_align).readlines():
    if line.find("SS_cons") >= 0:
        struct = line.split("SS_cons")[1].split(' ')[-1]
        SS_cons_apta += struct.rstrip('\n')
    if line.find("GC RF") >= 0:
        struct = line.split("RF")[1].split(' ')[-1]
        RF_apta += struct.rstrip('\n')

aptamer_start_read = len(SS_cons_apta)-overlap # infernal output such that only P1 is with () not <>. 
terminator_stop_read = overlap 

outfile_afa = open(snakemake.output[0],'w')
outfile_stk = open(snakemake.output[1],'w')

print("# STOCKHOLM 1.0\n",file=outfile_stk)

max_length = -1
for label in labels:
    if len(label)>max_length:
        max_length=len(label)


for label in labels:
    
    outfile_afa.write('>'+label+'\n')
    outfile_stk.write(label.ljust(max_length+1))

    # aligned aptamer extraction
    aligned_apta = ""
    for line in open(snakemake.input.aptamer_align).readlines():
        if line.find(label) >= 0 and line[0]!='#':
            print("aligned_apta", line.split(' ')[-1].rstrip('\n'))
            aligned_apta += line.split(' ')[-1].rstrip('\n')
    
    # aligned antiterminator extraction
    aligned_antiterminator = ""
    for line in open(snakemake.input.antiterminator_align).readlines():
        if line.find(label) >= 0 and line.find('#') < 0:
            print("aligned_anti", line.split(' ')[-1].rstrip('\n'))
            aligned_antiterminator += line.split(' ')[-1].rstrip('\n')

    # aligned terminator extraction
    aligned_terminator = ""
    for line in open(snakemake.input.terminator_align).readlines():
        if line.find(label) >= 0 and line.find('#') < 0:
            aligned_terminator += line.split(' ')[-1].rstrip('\n')

    # concatenation. what a nice line to write.
    aligned_line = aligned_apta[:aptamer_start_read]+aligned_antiterminator+aligned_terminator[terminator_stop_read:]

    outfile_afa.write(aligned_line+'\n')
    outfile_stk.write(aligned_line+'\n')

# GC SS_cons and RF lines #

# ss_cons and rf antiterm
SS_cons_antiterm = ""
RF_antiterm = ""
for line in open(snakemake.input.antiterminator_align).readlines():
    if line.find("SS_cons") >= 0:
        struct = line.split("SS_cons")[1].split(' ')[-1]
        SS_cons_antiterm += struct.rstrip('\n')
    if line.find("RF") >= 0:
        struct = line.split("RF")[1].split(' ')[-1]
        RF_antiterm += struct.rstrip('\n')

# ss_cons term
SS_cons_term = ""
RF_term = ""
for line in open(snakemake.input.terminator_align).readlines():
    if line.find("SS_cons") >= 0:
        struct = line.split("SS_cons")[1].split(' ')[-1]
        SS_cons_term += struct.rstrip('\n')
    if line.find("RF") >= 0:
        struct = line.split("RF")[1].split(' ')[-1]
        RF_term += struct.rstrip('\n')
outfile_afa.write(">SS_cons\n")
outfile_stk.write("#=GC SS_cons".ljust(max_length+1))
outfile_afa.write(SS_cons_apta+SS_cons_antiterm+SS_cons_term+'\n')
outfile_stk.write(SS_cons_apta+SS_cons_antiterm+SS_cons_term+'\n')
outfile_afa.write(">RF\n")
outfile_stk.write("#=GC RF".ljust(max_length+1))
outfile_afa.write(RF_apta+RF_antiterm+RF_term+'\n')
outfile_stk.write(RF_apta+RF_antiterm+RF_term+'\n')

outfile_afa.close()
outfile_stk.write('//')
outfile_stk.close()
