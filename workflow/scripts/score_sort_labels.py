label_score_list = []
for line in  open(snakemake.input[0]).readlines():
    if line[0]=='#':
        continue
    else:
        line = line.split(' ') 
        line = [s for s in line if s!='']
        label = line[1]
        score = line[6]
        label_score_list.append((label, score)) 


f = open(snakemake.output[0],'w')
cnt = 0
for label, score in sorted(label_score_list, key=lambda x: x[1]):
    f.write(label+','+score+'\n')
    cnt += 1
    print(cnt, int(len(label_score_list)/10), cnt%int(len(label_score_list)/10))
    if cnt%int(len(label_score_list)/10)==0:
        f.write("----- "+ str(100*cnt/float(len(label_score_list)))+ ' % of seqs above ----\n')

f.close()
