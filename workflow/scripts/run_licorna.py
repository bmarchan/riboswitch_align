import sys
import os
import subprocess


def run_licorna(query_file, target_file, td_file):

    length_query = len(open(query_file).readlines()[0])
    length_target = len(open(target_file).readlines()[0])

    cmd = "/usr/bin/time -f '%M_mrss_(kB),%U_user_time_(seconds)' "+ \
          "../LiCoRNA_fork/alignerDP -i "+query_file\
          +' -d '+td_file\
          +' -s '+target_file\
          +' -c '+str(abs(length_target-length_query))+' -n 2' 

    print(cmd)

    with open(snakemake.output[0], 'w') as f:
        f.write(cmd+'\n')
        f.write(query_file)
        f.write('\n')
        f.write(target_file)
        subprocess.call(cmd.split(' '), stdout=f, stderr=f)

if __name__ == "__main__":
    run_licorna(snakemake.input.query, snakemake.input.target, snakemake.input.td)
