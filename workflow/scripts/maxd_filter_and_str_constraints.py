import json

max_distance = snakemake.config["apta_terminator_max_distance"]

with open(snakemake.input.distance_dict) as f:
    dist_dict = json.load(f)

with open(snakemake.input.apta_dict) as f:
    apta_coord = json.load(f)

with open(snakemake.input.terminator_dict) as f:
    terminator_coord = json.load(f)

# alignment position to label
def labels(position, terminator=False):
    first_label = chr(ord('a')+int(position/676)+10*int(terminator)).upper()
    second_label = chr(ord('a')+int(position/26)).upper()
    third_label = str(position%10)
    return (first_label, second_label, third_label)

# aptamer consensus structure
SS_cons = ""
for line in open(snakemake.input.aptamer_align).readlines():
    if line.find("SS_cons") >= 0:
        struct = line.split("SS_cons")[1].split(' ')[-1]
        SS_cons += struct.rstrip('\n')

# terminator consensus structure
fasta_lines = open(snakemake.input.terminator_align).readlines()

last_line = fasta_lines[-1]
last_part = last_line.split('alifold')[1].split(' ')
last_part = [s for s in last_part if s!='']
term_alifold = last_part[0]

# label selection
selected_labels = []
for line in open(snakemake.input.unaligned_fa):
    if line[0]=='>':
        label = line[1:].split(' ')[0]
        try:
            if dist_dict[label] <= max_distance:
                selected_labels.append(label)
        except KeyError:
            continue

## aptamer only file
#apta_only = open(snakemake.output[1],'w')
#
#for line in open(snakemake.input.aptamer_unaligned).readlines():
#    if line[0]=='>':
#        label = line[1:].split(' ')[0]
#        take_seq = label in selected_labels
#        if take_seq:
#            apta_only.write(line)
#
#    else:
#        if take_seq:
#            apta_only.write(line)
#
#apta_only.close()

# actual writing of main output file
outfile = open(snakemake.output[0],'w')

# filter for debug purposes PUT HIGH VALUE 
# IN CONFIG FILE FOR FINAL RUNS
selected_labels = selected_labels[:snakemake.config["global_num_seq_limit"]]

for line in open(snakemake.input.unaligned_fa).readlines():
    if line[0]=='>':
        label = line[1:].split(' ')[0]
        take_seq = label in selected_labels
        if take_seq:
            outfile.write(line)

    else:
        if take_seq:

            # position marking end of terminator
            max_pos = int(terminator_coord[label].split('/').split('-')[1])

            seq_line = line[:max_pos].rstrip('\n')+'\n'
            outfile.write(seq_line)
            
            # constraint lines, aptamer part
            aligned_apta = ""
            for line in open(snakemake.input.aptamer_align).readlines():
                if line.find(label) >=0 and line[0]!='#':
                    aligned_apta += line.split(' ')[-1].rstrip('\n')
            
            structure_line = ""
            constraint_line1 = ""
            constraint_line2 = ""
            constraint_line3 = ""


            length_tracker = 0
            for k in range(len(aligned_apta)):
                length_tracker += 1
                if aligned_apta[k] not in ['.','-']:
                    structure_line += SS_cons[k]
                    constraint_line1 += labels(k)[0]
                    constraint_line2 += labels(k)[1]
                    constraint_line3 += labels(k)[2]

            # constraint lines, terminator part
            pos = int(terminator_coord[label].split('-')[0])
            terminator_start = pos + 10 # that's just the way things are.

            # if not long enough, pad up to the beginning of the terminator
            while len(structure_line) < terminator_start:
                structure_line += '.'
                constraint_line1 += '.'
                constraint_line2 += '.'
                constraint_line3 += '.'

            # if too long, then crop to beginning of terminator.
            structure_line = structure_line[:terminator_start]
            constraint_line1 = constraint_line1[:terminator_start]
            constraint_line2 = constraint_line2[:terminator_start]
            constraint_line3 = constraint_line3[:terminator_start]

            # extracting aligned terminator
            aligned_terminator = ""
            for line in open(snakemake.input.terminator_align).readlines():
                if line.find(label) >= 0: 
                    aligned_terminator += line.split(' ')[-1].rstrip('\n')

            # entering aligned terminator constraint
            n_open = term_alifold.count('(') 
            for k in range(len(aligned_terminator)):
                if aligned_terminator[k]!='-':
                    if term_alifold[:k].count('(')==n_open and term_alifold[:k].count(')')==0:
                        structure_line += 'x'
                    else:
                        structure_line += term_alifold[k] 
                    constraint_line1 += labels(k, terminator=True)[0] 
                    constraint_line2 += labels(k, terminator=True)[1] 
                    constraint_line3 += labels(k, terminator=True)[2] 

            # final padding to entire structure
            while len(structure_line) < len(seq_line)-1:     
                structure_line += '.'
                constraint_line1 += '.'
                constraint_line2 += '.'
                constraint_line3 += '.'


            # making sure consistent
            structure_line = structure_line.replace('<','(')
            structure_line = structure_line.replace('>',')')
            
            so = []
            unpaired_closing = []

            for k, c in enumerate(structure_line):
                if c=='(':
                    so.append(k)
                if c==')':
                    if len(so) > 0:
                        so.pop()
                    else:
                        unpaired_closing.append(k)

            new_structure_line = ""
            for k, c in enumerate(structure_line):
                if k in so+unpaired_closing:
                    new_structure_line += '.'
                else:
                    new_structure_line += structure_line[k]
            
            new_structure_line += ' #S\n'

            outfile.write(new_structure_line.replace('-','.').replace('_','.').replace(',','.').replace('~','.'))
            outfile.write(constraint_line1+' #1\n')
            outfile.write(constraint_line2+' #2\n')
            outfile.write(constraint_line3+' #3\n')

outfile.close()
