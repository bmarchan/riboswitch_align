# extracting terminator seq coordinates in neat dict.
import json
with open(snakemake.input.terminator_coord) as f:
    terminator_coord = json.load(f)

# filtered fasta file:
outfile = open(snakemake.output[0], 'w')

# overlap
overlap = snakemake.config["overlap_antiterminator"]

# for constraints writing
def labels(position, terminator=False):
    first_label = chr(ord('a')+int(position/10)+int(terminator)*10).upper()
    second_label = str(position%10)
    print(position, first_label, second_label, int(position/10))
    return (first_label, second_label)

# selected labels and aligned anti extraction
selected_labels = set([])
aligned_anti = {}

for line in open(snakemake.input.aligned_anti).readlines():
    if line.find('NC')>=0:
        label = line.split(' ')[0]
        selected_labels.add(label)
        try:
            aligned_anti[label] += line.split(' ')[-1].rstrip('\n')
        except KeyError:
            aligned_anti[label] = line.split(' ')[-1].rstrip('\n')

# actual file writing
for line in open(snakemake.input.input_fa).readlines():
    if line[0]=='>':
        label = line[1:].split(' ')[0]

        # filtering
        take_seq = label in selected_labels

    else:
        if take_seq:

            # sequence line writing
            start_pos = int(terminator_coord[label].split('/')[0].split('-')[0])
            end_pos = int(terminator_coord[label].split('/')[0].split('-')[1])
            outfile.write('>'+label+'\n')
            seq_line = line[start_pos+10:end_pos].rstrip('\n')+'\n'
            outfile.write(seq_line)

            # constraint lines building
            constraint1 = ""
            constraint2 = ""
        
            # when to start reading antiterminator alignment ?
            # have to find when overlap region starts in aligned anti
            aligned_anti_start_read = len(aligned_anti[label])

            # going backwards counting actual letters
            num_actual_letters = 0
            while num_actual_letters < overlap:
                aligned_anti_start_read -= 1
                print(label)
                print(aligned_anti[label], len(aligned_anti[label]), aligned_anti_start_read)
                if aligned_anti[label][aligned_anti_start_read]!='-':
                    num_actual_letters += 1

            # actual constraint writing:
            for k in range(aligned_anti_start_read, len(aligned_anti[label]),1):
                if aligned_anti[label][k]!='-':
                    constraint1 += labels(k)[0]
                    constraint2 += labels(k)[1]

            # padding constraints with gaps
            while len(constraint1) < len(seq_line)-1:
                constraint1 += '.'
                constraint2 += '.'

            # writing them
            outfile.write(constraint1+' #1\n')
            outfile.write(constraint2+' #2\n')

outfile.close()
