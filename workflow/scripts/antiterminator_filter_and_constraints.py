import json

max_distance = snakemake.config["apta_terminator_max_distance"]
overlap = snakemake.config["overlap_antiterminator"]

with open(snakemake.input.distance_dict) as f:
    dist_dict = json.load(f)

with open(snakemake.input.apta_dict) as f:
    apta_coord = json.load(f)

with open(snakemake.input.terminator_dict) as f:
    terminator_coord = json.load(f)

# aptamer consensus structure
SS_cons = ""
for line in open(snakemake.input.aptamer_align).readlines():
    if line.find("SS_cons") >= 0:
        struct = line.split("SS_cons")[1].split(' ')[-1]
        SS_cons += struct.rstrip('\n')

print(SS_cons, len(SS_cons))
aptamer_start_read = len(SS_cons)-overlap # infernal output such that only P1 is with () not <>. 
print(aptamer_start_read)

# label selection
selected_labels = [label for label in dist_dict.keys() if dist_dict[label] <= max_distance]

# for constraints writing
def labels(position, terminator=False):
    first_label = chr(ord('a')+int(position/10)+int(terminator)*10).upper()
    second_label = str(position%10)
    print(position, first_label, second_label, int(position/10))
    return (first_label, second_label)

# actual writing of main output file
outfile = open(snakemake.output[0],'w')

for label in selected_labels:

    # unaligned seq extraction:
    unaligned_seq = ""
    for line in open(snakemake.input.unaligned_fa).readlines():
        if line[0]=='>':
            candidate_label = line[1:].split(' ')[0]
            take_seq = candidate_label == label
        else:
            if take_seq:
                unaligned_seq = line.rstrip('\n')

    # aligned aptamer extraction
    aligned_apta = ""
    for line in open(snakemake.input.aptamer_align).readlines():
        if line.find(label) >= 0 and line[0]!='#':
            aligned_apta += line.split(' ')[-1].rstrip('\n')


    # aptamer start read in unaligned seq
    aptamer_start_read_unaligned = 0
    print(label, "aligned apta",aligned_apta)
    for k in range(aptamer_start_read):
        if aligned_apta[k] not in ['.','-']:
            aptamer_start_read_unaligned += 1

    # terminator pos, in unaligned seq (as per arnold output)
    terminator_start = int(terminator_coord[label].split('/')[0].split('-')[0])+10
    
    # terminator stop read in unaligned
    terminator_stop_read_unaligned = terminator_start + overlap

    # should not happend too much but who knows
    if aptamer_start_read_unaligned >= terminator_stop_read_unaligned:
        continue

    seq_line = unaligned_seq[aptamer_start_read_unaligned:terminator_stop_read_unaligned]+'\n'
    constraint_line1 = ""
    constraint_line2 = ""

    # constraint lines, aptamer part
    for k in range(aptamer_start_read, len(aligned_apta), 1):
        if aligned_apta[k] not in ['.','-']:
            constraint_line1 += labels(k-aptamer_start_read)[0] 
            constraint_line2 += labels(k-aptamer_start_read)[1] 
    
    # pad up to the end
    while len(constraint_line1) < len(seq_line)-1:
        constraint_line1 += '.'
        constraint_line2 += '.'



    outfile.write('>'+label+' unspecified species\n')
    outfile.write(seq_line)   
    if overlap >= 0:
        outfile.write(constraint_line1+' #1\n')
        outfile.write(constraint_line2+' #2\n')

outfile.close()
