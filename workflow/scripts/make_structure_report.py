from varnaapi import VARNA

thresh = snakemake.config["number_of_deciles_to_throw"]
l = snakemake.config["length_after_apta"]

latex_file = open(snakemake.output[0],'w')

latex_file.write('\documentclass{article}\n')
latex_file.write('\\usepackage{graphicx}\n')
latex_file.write('\\begin{document}\n')

latex_file.write('\paragraph{Description} For each sequence, the label of the sequence, then the alifold restricted to non-gap positions in the aligned sequence, and finally a VARNA representation of the alifold restricted to non-gap positions are represented.')

latex_file.write('\paragraph{}')

fasta_lines = open(snakemake.input[0]).readlines()

# gathering alifold
alifold = ""
reading = False
for line in fasta_lines[:-1]:
    if line.find('alifold') >= 0:
        reading=True
    if reading:
        alifold += line.split(' ')[-1].rstrip('\n')

# funny stuff on last line
last_line = fasta_lines[-1]
last_part = last_line.split(' ')
for part in last_part:
    if part!='':
        alifold+=part
        break

print(alifold)

# gathering labels
labels = set([])

for line in fasta_lines[8:]:
    label = line.split(' ')[0]
    if label[:2]=='NC':
        labels.add(label)

# filling the tex file
for label in list(labels):
    alignment = ''
    for line in fasta_lines:
        if line.split(' ')[0]==label:
            alignment += line.split(' ')[-1]

    non_gap_sub_alifold = ""
    
    for k in range(min(len(alignment),len(alifold))):
        if alignment[k]!='-':
            non_gap_sub_alifold += alifold[k]

    # removing unbalanced parenthesis
    stack = []
    unpaired_closing = []
    for k, c in enumerate(non_gap_sub_alifold):
        if c=='(':
            stack.append(k)
        if c==')':
            if len(stack) > 0:
                stack.pop()
            else:
                unpaired_closing.append(k)

    final_sub_alifold = ""
    for k in range(len(non_gap_sub_alifold)):
        if k in stack+unpaired_closing:
            final_sub_alifold+='.'
        else:
            final_sub_alifold+=non_gap_sub_alifold[k]

    # making varna picture
    print(final_sub_alifold)
    
#    v = VARNA(structure=final_sub_alifold)
    figname = "./structures/"+label+"_thresh"+str(thresh)+"_l"+str(l)+".eps"
#    v.savefig("results/"+figname)

    latex_file.write(label.replace('_','\_').replace(':','.')+'\\newline   '+final_sub_alifold+'\n\includegraphics[width=.5\\textwidth]{'+figname+'}\\newline\n')

latex_file.write('\end{document}\n')
