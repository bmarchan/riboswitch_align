number_of_deciles_to_throw = snakemake.config["number_of_deciles_to_throw"]
max_length_after_apta = snakemake.config["length_after_apta"]

SS_cons = ""
for line in open(snakemake.input[3]).readlines():
    if line.find("SS_cons") >= 0:
        struct = line.split("SS_cons")[1].split(' ')[-1]
        SS_cons += struct.rstrip('\n')

selected_labels = []

cnt_decile = 0
for line in open(snakemake.input[1]).readlines():
    if line.find('---') >= 0:
        cnt_decile += 1
    else:
        if cnt_decile >= number_of_deciles_to_throw:
            selected_labels.append(line.split(',')[0])

print("selected_labels", selected_labels)

outfile = open(snakemake.output[0], 'w')        

import json
apta_coord = {}
with open(snakemake.input[2]) as f:
    apta_coord = json.load(f)

for line in open(snakemake.input[0]).readlines():
    if line[0]=='>':
        label = line[1:].split(' ')[0]
        take_seq = label in selected_labels
        if take_seq:
            outfile.write(line)
    else:
        if take_seq:
            apta_length = int(apta_coord[label].split('-')[1])-int(apta_coord[label].split('-')[0])
            seq_line = line[:(apta_length+max_length_after_apta)].rstrip('\n')+'\n'
            outfile.write(seq_line)
            
            aligned_apta = ""
            for line in open(snakemake.input[3]).readlines():
                if line.find(label) >=0 and line[0]!='#':
                    aligned_apta += line.split(' ')[-1].rstrip('\n')
            
            constraint_line = ""

            for k in range(len(aligned_apta)):
                if aligned_apta[k] not in ['.','-']:
                    constraint_line += SS_cons[k]
            
            while len(constraint_line) < len(seq_line)-1:     
                constraint_line += '.'

            constraint_line += ' #S\n'
            constraint_line = constraint_line.replace('<','(')
            constraint_line = constraint_line.replace('>',')')

            so = []

            for k, c in enumerate(constraint_line):
                if c=='(':
                    so.append(k)
                if c==')':
                    so.pop()

            new_constraint_line = ""
            for k, c in enumerate(constraint_line):
                if k in so:
                    new_constraint_line += '.'
                else:
                    new_constraint_line += constraint_line[k]

            outfile.write(new_constraint_line.replace('-','.').replace('_','.').replace(',','.').replace('~','.'))


outfile.close()
