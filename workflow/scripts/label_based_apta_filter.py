# label selection
selected_labels = []

for line in open(snakemake.input.labels_file).readlines():
    if line[0]=='>':
        selected_labels.append(line[1:].split(' ')[0])


# filering the other file with selected labels
outfile = open(snakemake.output[0],'w')

for line in open(snakemake.input.aptamer_unaligned):
    if line[0]=='>':
        take_seq = line[1:].split(' ')[0] in selected_labels
        if take_seq:
            outfile.write(line)
    else:
        if take_seq:
            outfile.write(line)

outfile.close()
