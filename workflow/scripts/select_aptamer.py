# extracting aptamer gen coordinates in neat dict.
import json
with open(snakemake.input[1]) as f:
    aptamer_coord = json.load(f)

# filtered fasta file:
outfile = open(snakemake.output[0], 'w')

for line in open(snakemake.input[0]).readlines():
    if line[0]=='>':
    # new sequence, extracting label and coordinates
        label = line[1:].split(' ')[0]

        #low-up format
        current_coord = line.split(':')[1].split(' ')[0]

        cur_down = int(current_coord.split('-')[0])
        cur_up = int(current_coord.split('-')[1])

        # writing line in filtered file, keeping same label
        outfile.write(line)
    else:
    # actual sequence
        apta_down = int(aptamer_coord[label].split('-')[0])
        apta_up = int(aptamer_coord[label].split('-')[1])

        outfile.write(line.rstrip('\n')[(apta_down-cur_down):(apta_up-cur_down+1)]+'\n')

outfile.close()
