from optparse import OptionParser
import json
import os

sep = 'A'

def convert_to_gr():


    outfile = open(snakemake.output[0], 'w')

    dgf_file = open(snakemake.input[0])  

    dgf_lines = dgf_file.readlines()

    vertices = set([])
    edges = set([])


    for line in dgf_lines[1:]:
        nt1 = line.split(' ')[1]
        nt2 = line.split(' ')[2].rstrip('\n')

        vertices.add(nt1)
        vertices.add(nt2)

    index2vertex = {}
    vertex2index = {}

    for k, v in enumerate(sorted(list(vertices))):
    
        index2vertex[k] = v
        vertex2index[v] = k

    with open(snakemake.output[1],'w') as i2v_file:
        json.dump(index2vertex, i2v_file)
    
    with open(snakemake.output[2],'w') as v2i_file:
        json.dump(vertex2index, v2i_file)

    for line in dgf_lines[1:]:

        nt1 = line.split(' ')[1]
        nt2 = line.split(' ')[2].rstrip('\n')

        print(nt1, nt2, vertex2index[nt1], vertex2index[nt2])

        edges.add((min(vertex2index[nt1],vertex2index[nt2]), max(vertex2index[nt1],vertex2index[nt2])))

    outfile.write('p tw '+str(len(vertices))+' '+str(len(edges))+'\n')

    for u,v in sorted(edges):
        outfile.write(str(u+1)+' '+str(v+1)+'\n')

if __name__ == "__main__":
    convert_to_gr()
