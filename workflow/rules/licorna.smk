rule split_into_target_files:
    input:
        "resources/{filename}"
    output:
        "results/splitting_{filename}.done"
    log:
        "results/miscellani_logs/splitting_{filename}.log"
    conda:
        "envs/alignment_env.yml"
    script:
        "../scripts/split_into_target_files.py"

rule produce_seq_file:
    input:
        "resources/{structure}"
    output:
        "results/query_files/{structure}.seq"
    log:
        "results/miscellani_logs/seq_file_creation_{structure}.log"
    conda:
        "envs/alignment_env.yml"
    script:
        "../scripts/produce_seq_file.py"

rule produce_dgf_file:
    input:
        "results/query_files/{structure}.seq"
    output:
        "results/dgf_files/{structure}.dgf"
    conda:
        "envs/alignment_env.yml"
    log:
        "results/miscellani_logs/dgf_file_creation_{structure}.log"
    shell:
        "./workflow/scripts/RNAML2dgf -i {input} >> {output}"

rule dgf_to_gr:
    input:
        "results/dgf_files/{structure}.dgf"
    output:
        "results/gr_files/{structure}.gr",
        "results/gr_files/{structure}_i2v.json",
        "results/gr_files/{structure}_v2i.json"
    conda:
        "envs/alignment_env.yml"
    log:
        "results/miscellani_logs/gr_conversion_{structure}.log"
    script:
        "../scripts/convert_dgf_to_gr.py" 


rule tamaki_solver:
    input:
        "results/gr_files/{structure}.gr"
    output:
        "results/pace_td_files/{structure}.td"
    conda:
        "envs/alignment_env.yml"
    log:
        "results/miscellani_logs/td_computation_log_{structure}.log"
    shell:
        'java -cp workflow/scripts/PACE2017-TrackA -Xmx3g -Xms3g -Xss10m tw.exact.MainDecomposer < {input} > {output}'
    
rule tree_decomposition_format_conversion:
    input:
        td = "results/pace_td_files/{structure}.td",
        i2v = "results/gr_files/{structure}_i2v.json",
        v2i = "results/gr_files/{structure}_v2i.json"
    output:
        "results/{structure}.td1.dot"
    conda:
        "envs/alignment_env.yml"
    log:
        "results/miscellani_logs/td_conversion_{structure}.log"
    script:
        "../scripts/from_pace_td_to_td1dot.py"
