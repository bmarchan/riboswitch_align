import os

extension_len = 100

f = open('../RF00162_extended.fa','w')

not_found = []

for line in open('../RF00162.fa').readlines():#[:30]:
    if line[0]=='>':
        print(line)
        accession = line.split('>')[1].split('/')[0]

        acc_range = line.split('/')[1].split(' ')[0].split('-')
        acc_range = [int(x) for x in acc_range]

        reverse_complement = acc_range[0]>=acc_range[1]
        print(acc_range, reverse_complement)

        if reverse_complement:
            print(line.replace(str(acc_range[0]),str(acc_range[1]-extension_len)).rstrip('\n'),file=f)
            new_range = [acc_range[0],acc_range[1]-extension_len]
        else:
            print(line.replace(str(acc_range[0]),str(acc_range[1]+extension_len)).rstrip('\n'),file=f)
            new_range = [acc_range[0],acc_range[1]+extension_len]

        new_range = [str(x) for x in new_range]

        found = False
        for name in os.listdir():
            if name.find('.py') >= 0:
                continue
            if name[:5]!=accession[:5]:
                continue
            read = False
            for line2 in open(name).readlines():
                if line2.find(accession) >= 0:
                    found = True
                    read = True
                    acc = ""
                    continue
                if read:
                    acc += line2.rstrip('\n')
                if line2[0]=='>':
                    read = False
                    
        new_range = [int(x) for x in new_range]
        print(new_range)
        print("extended:")
        if not found:
            not_found.append(accession)
            continue
        if not reverse_complement:
            print(acc[new_range[0]-1:new_range[1]])
            print(acc[new_range[0]-1:new_range[1]],file=f)
        else:
            s = acc[new_range[1]-1:new_range[0]]
            s = s[::-1]
            s = s.replace('A','X')
            s = s.replace('T','A')
            s = s.replace('X','T')
            s = s.replace('G','X')
            s = s.replace('C','G')
            s = s.replace('X','C')
            print(s)
            print(s,file=f)

    else:
        print("original")
        print(line)
        #input()

print("not found", not_found)
