from Bio import Entrez
import requests_cache
requests_cache.install_cache()

extension_len = 200

f = open('../RF00162_extended_other.fa','w')

cnt = 0
N_seq = len(open('../RF00162.fa').readlines())

# Replace the email address below with your own
Entrez.email = "bertrand.marchand7@gmail.com"

for line in open('../RF00162.fa').readlines():
    cnt += 1
    print(cnt,"out of ",N_seq)
    if line[0]=='>':
        accession = line[1:].split(' ')[0].split('/')[0] 
        print(accession)
        acc_range = line.split('/')[1].split(' ')[0].split('-')
        acc_range = [int(x) for x in acc_range]

        reverse_complement = acc_range[0]>=acc_range[1]
        handle = Entrez.efetch(db='nucleotide',id=accession,rettype="fasta")
        #print(handle.read())
        fasta = handle.read()
        
        if reverse_complement:
            print(line.replace(str(acc_range[0]),str(acc_range[1]-extension_len)).rstrip('\n'),file=f)
            new_range = [acc_range[0],acc_range[1]-extension_len]
        else:
            print(line.replace(str(acc_range[0]),str(acc_range[1]+extension_len)).rstrip('\n'),file=f)
            new_range = [acc_range[0],acc_range[1]+extension_len]


        acc = ""
        for line in fasta.split('\n')[1:]:
            acc += line

        new_range = [int(x) for x in new_range]
        print(new_range)
        print("extended:")
        if not reverse_complement:
            print(acc[new_range[0]-1:new_range[1]])
            print(acc[new_range[0]-1:new_range[1]],file=f)
        else:
            s = acc[new_range[1]-1:new_range[0]]
            s = s[::-1]
            s = s.replace('A','X')
            s = s.replace('T','A')
            s = s.replace('X','T')
            s = s.replace('G','X')
            s = s.replace('C','G')
            s = s.replace('X','C')
            print(s)
            print(s,file=f)

    else:
        print("original")
        print(line)
        #input()

