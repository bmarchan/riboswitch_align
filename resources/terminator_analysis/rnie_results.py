import json

with open('accession_identifier.json') as f:
    mapping = json.load(f)

has_terminator = {}
terminator_position = {}

def increase_range(acc_range):
    a = acc_range.split('/')[1].split('-')[0]
    b = acc_range.split('/')[1].split('-')[1].rstrip('\n')
    if int(a)>=int(b):
        return acc_range.replace(b,str(int(b)+100))
    if int(a)<int(b):
        return acc_range.replace(b,str(int(b)-100))

for line in open('../RNIE/synthesized_natural_sequences_extended-geneMode.tabfile').readlines():
    if line[0]!='#':
        res = line.split(' ')
        res = [c for c in res if c!='']
        print(res, increase_range(res[1]))
        if int(res[2])<=int(res[3]):
            has_terminator[increase_range(res[1])] = 'yes'
            terminator_position[increase_range(res[1])] = '-'.join([res[2],res[3]]) 

f = open('rnie_terminator_detection_results.txt','w')

print('id,accession/range,terminator_detection,terminator_pos',file=f)

for line in open('2022-11-15-aptamer-with-retrieved-organisms-ncbi.tsv').readlines()[1:]:
    seq = line.split('\t')
    if seq[3][:6]=='APSAMN':
        if int(seq[3][6:])>=1 and int(seq[3][6:]) <=276:
            print(seq[2])
            if seq[2] not in has_terminator.keys():
                has_terminator[seq[2]] = 'no'
                terminator_position[seq[2]] = 'NA'
            print(','.join([seq[3],seq[2],has_terminator[seq[2]],terminator_position[seq[2]]]),file=f)
