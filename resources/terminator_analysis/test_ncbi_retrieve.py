import requests

# Set accession number and range
accession_number = "LT985709"
start_pos = 500
end_pos = 1000

# Set the base URL for the ENA API
base_url = "https://www.ebi.ac.uk/ena/browser/api"

# Define the search parameters
params = {
    "accession": accession_number,
    "result": "fasta",
    "start": start_pos,
    "end": end_pos
}

# Use the requests module to send a GET request to the ENA API
response = requests.get(base_url, params=params)

# Extract the sequence from the response content
sequence = response.content.decode().strip()

# Print the sequence
print(sequence)
