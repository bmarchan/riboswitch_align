import json

has_terminator = {}
terminator_position = {}

for line in open('../RNIE/RF00162_extended-geneMode.tabfile').readlines():
    if line[0]!='#':
        res = line.split(' ')
        res = [c for c in res if c!='']
        if int(res[2])<=int(res[3]):
            has_terminator[res[1]] = 'yes'
            terminator_position[res[1]] = '-'.join([res[2],res[3]]) 

f = open('rnie_RF00162_terminator_detection_results.txt','w')

print('accession/range,terminator_detection,terminator_pos',file=f)

for line in open('RF00162_extended.fa').readlines()[1:]:
    if line[0]=='>':
        acc = line.split('>')[1].split(' ')[0]
        if acc not in has_terminator.keys():
            has_terminator[acc] = 'no'
            terminator_position[acc] = 'NA'
        print(','.join([acc,has_terminator[acc],terminator_position[acc]]),file=f)
