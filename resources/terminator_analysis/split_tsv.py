import json

f = open('synthesized_natural_sequences.fa','w')

acc_id_map = {}

for line in open('2022-11-15-aptamer-with-retrieved-organisms-ncbi.tsv').readlines():
    identifier = line.split('\t')[3]
    if identifier.find('APSAMN') >= 0:
        if int(identifier[6:]) <= 276:
            print(line.split('\t'))
            print(line.split('\t')[2])
            print(identifier)
            acc_and_range = line.split('\t')[2]
            acc_id_map[acc_and_range] = identifier
            desc = line.split('\t')[-1].rstrip('\n')
            print('>'+acc_and_range+' '+desc, file=f)
            print(line.split('\t')[4],file=f)

f.close()

with open('accession_identifier.json','w') as f:
    json.dump(acc_id_map, f)
