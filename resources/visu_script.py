
import json

with open('rnie_plus_strand_seqs_terminator_coord.json') as f:
    rnie = json.load(f)
rnie_keys = set(rnie.keys())

with open('plus_strand_seqs_terminator_coord.json') as f:
    arnold = json.load(f)
arnold_keys = set(rnie.keys())

with open('transterm_plus_strand_seqs_terminator_coord.json') as f:
    transterm = json.load(f)
transterm_keys = set(rnie.keys())

f = open('visu_term.fa','w')


for line in open('plus_strand_seqs.fa').readlines(): 
    if line[0]=='>': 
        label = line[1:].split(' ')[0] 
    else: 
        if label in transterm_keys.union(rnie_keys).union(arnold_keys): 
            rnie_line = ['-' for _ in range(len(line))] 
            transterm_line = ['-' for _ in range(len(line))] 
            arnold_line = ['-' for _ in range(len(line))] 
            try: 
                for hit in arnold[label].split('/'): 
                    for k in range(int(hit.split('-')[0]),int(hit.split('-')[1]),1): 
                        arnold_line[k]='A' 
                f.write('>'+label+'arnold\n') 
                for s in arnold_line: 
                    f.write(s) 
                f.write('\n') 
            except KeyError: 
                pass 
            try:  
                for hit in rnie[label].split('/'):  
                    for k in range(int(hit.split('-')[0]),int(hit.split('-')[1]),1):  
                        rnie_line[k]='U'  
                f.write('>'+label+'rnie\n') 
                for s in rnie_line: 
                    f.write(s) 
                f.write('\n') 
            except KeyError:  
                pass  
            try: 
                for hit in transterm[label].split('/'): 
                    for k in range(int(hit.split('-')[0]),int(hit.split('-')[1]),1): 
                        transterm_line[k] = 'G' 
                f.write('>'+label+'transterm\n') 
                for s in transterm_line: 
                    f.write(s) 
                f.write('\n')
            except KeyError: 
                pass 
